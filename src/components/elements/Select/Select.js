import React from 'react';
import './Select.css';
import PropTypes from 'prop-types';

export default function Select({
  containerStyle,
  label,
  labelStyle,
  onChangeSelect,
  selectStyle,
  value,
}) {
  return (
    <div className={containerStyle}>
      <label className={labelStyle}>{label}</label>
      <select className={selectStyle} onChange={onChangeSelect} value={value}>
        <option value="">All Category</option>
        <option value="Ready">Ready</option>
        <option value="Barang Bekas">Barang Bekas</option>
        <option value="Pre-Order">Pre-Order</option>
      </select>
    </div>
  );
}

Select.propTypes = {
  containerStyle: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  labelStyle: PropTypes.string.isRequired,
  onChangeSelect: PropTypes.func.isRequired,
  selectStyle: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
};
