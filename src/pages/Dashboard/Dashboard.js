import React from 'react';
import './Dashboard.css';

export default function Dashboard() {

  return (
    <main className="h-full w-full bg-white flex justify-center items-center text-[#1E293B]">
      <h1 className="text-3xl font-bold">Welcome To WKWK Land</h1>
    </main>
  );
}
