import React from 'react';
import PropTypes from 'prop-types';
import { hot } from 'react-hot-loader/root';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Provider } from 'react-redux';
import ErrorBoundary from './components/elements/ErrorBoundary';
import { Belajar, Products, Users, ProductDetail, NewProduct, Dashboard } from './pages';
import Layouts from './components/Layouts/Layouts';

const App = ({ store }) => {
  return (
    <ErrorBoundary>
      <Provider store={store}>
        <BrowserRouter>
          <Layouts>
            <Routes>
              <Route element={<Dashboard />} exact path="/" />
              <Route element={<Belajar />} exact path="/Belajar" />
              <Route element={<Products />} exact path="/Products" />
              <Route element={<ProductDetail />} exact path="/ProductDetail/:id" />
              <Route element={<Users />} exact path="/Users" />
              <Route element={<NewProduct />} exact path="/Products/AddNewProduct" />
            </Routes>
          </Layouts>
        </BrowserRouter>
      </Provider>
    </ErrorBoundary>
  );
};

export default hot(App);

App.propTypes = {
  store: PropTypes.object.isRequired,
};
